package com.example.plusgameproject

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.example.plusgameproject.databinding.FragmentGameMinusBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [GamePlusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

class GameMinusFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentGameMinusBinding
    private lateinit var btnSum1: Button
    private lateinit var btnSum2: Button
    private lateinit var btnSum3: Button
    private lateinit var textCountWin: TextView
    private lateinit var textCountLose: TextView
    private lateinit var textNumberFirst: TextView
    private lateinit var textNumberSecond: TextView
    private var countWin = 0
    private var countLose = 0
    private var sum = 0
    private var intent = Intent()
    private var num1 = 0
    private var num2 = 0
    private var randomBtn = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_game_minus, container, false)
        startGame()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun startGame() {
        game()
        check()
        resultCount()

    }

    private fun check() {

        btnSum1 = binding.btnSum1
        btnSum2 = binding.btnSum2
        btnSum3 = binding.btnSum3
        randomBtn = Random.nextInt(1, 4)
        Log.i(TAG, "Random Button $randomBtn")

        if (randomBtn == 1) {
            binding.apply {
                btnSum1.text = sum.toString()
                btnSum2.text = (sum + 1).toString()
                btnSum3.text = (sum + 2).toString() }
        }
        if (randomBtn == 2) {
            binding.apply {
                btnSum1.text = (sum - 1).toString()
                btnSum2.text = sum.toString()
                btnSum3.text = (sum + 1).toString()
            }
        }
        if (randomBtn == 3) {
            binding.apply {
                btnSum1.text = (sum - 2).toString()
                btnSum2.text = (sum - 1).toString()
                btnSum3.text = sum.toString()
            }
        } else {
            binding.apply {
                btnSum1.setOnClickListener {
                    if (btnSum1.text == sum.toString()) {
                        countWin += 1
                        startGame()
                    } else {
                        countLose += 1
                        startGame()
                    }
                }
                btnSum2.setOnClickListener {
                    if (btnSum2.text == sum.toString()) {
                        countWin += 1
                        startGame()
                    } else {
                        countLose += 1
                        startGame()
                    }
                }
                btnSum3.setOnClickListener {
                    if (btnSum3.text == sum.toString()) {
                        countWin += 1
                        startGame()
                    } else {
                        countLose += 1
                        startGame()
                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun resultCount() {
        textCountWin = binding.textCountWin
        textCountLose = binding.textCountLose
        binding.apply {
            textCountWin.text = "Win: $countWin"
            textCountLose.text = "Lose: $countLose"
        }
    }

    private fun game(){
        textNumberFirst = binding.textNumberFirst
        textNumberSecond = binding.textNumberSecond
        num1 = Random.nextInt(1, 11)
        num2 = Random.nextInt(1, 11)
        Log.i(TAG, "Num 1 $num1")
        Log.i(TAG, "Num 2 $num2")
        binding.apply {
            textNumberFirst.text = num1.toString()
            textNumberSecond.text = num2.toString()
            sum = num1 - num2
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment GamePlusFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            GamePlusFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}